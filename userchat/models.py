from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.six import python_2_unicode_compatible
from channels import Group
import json
from .settings import MSG_TYPE_MESSAGE


# Create your models here.
@python_2_unicode_compatible
class ChatRoom(models.Model):
    title = models.TextField()
    participants = models.ManyToManyField(User)

    def __str__(self):
        return self.title

    def get_room_messages(self):
        listms=Message.objects.all().filter(id=self.id)
        #return (json.dumps(listms))

    @property
    def websocket_group(self):
        return Group("chat-%s" % self.id)

    def send_message(self, message, user, msg_type=MSG_TYPE_MESSAGE):
        """
        Called to send a message to the room on behalf of a user.
        """

        if message != None:
            mess = Message.objects.create(
                message=message,
                sender=user,
                sent_time=timezone.now(),
                recipient=ChatRoom.objects.get(id=self.id)
            )
            mess.save()
        else:
            listmes = Message.objects.filter(recipient=self.id)

        final_msg = {'room': str(self.id), 'message': message, 'username': user.username, 'msg_type': msg_type}

        # Send out the message to everyone in the room
        self.websocket_group.send(
            {"text": json.dumps(final_msg)}
        )


@python_2_unicode_compatible
class Message(models.Model):
    message = models.TextField()
    sender = models.ForeignKey(User, related_name='From_User')
    sent_time = models.DateTimeField(default=timezone.now)
    read_time = models.DateTimeField(null=True, blank=True)
    choices = (
        (0, 'unread'),
        (1, 'read')
    )
    status = models.IntegerField(default=0, choices=choices)
    recipient = models.ForeignKey(ChatRoom)

    def __str__(self):
        return 'message: {1}: {0}'.format(self.message, self.sender.first_name)
