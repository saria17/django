from django.conf.urls import url
from userchat import views
urlpatterns=[
    url(r'^$', views.main_page, name='index'),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', views.logout_page)

    ]