from django.shortcuts import render
from django.contrib.auth import logout
from django.template import loader
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import ChatRoom

# Create your views here.
def main_page(request):
    users = User.objects.all()
    for user in users:
        print(user.get_full_name())
    context = {
        'users': users,
        'user': request.user
    }

    return render(request, 'userchat/mainpage.html', context)


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/userchat/')


@login_required
def index(request):
    """
    Root page view. This is essentially a single-page app, if you ignore the
    login and admin parts.
    """
    # Get a list of rooms, ordered alphabetically
    rooms = ChatRoom.objects.order_by("title")

    # Render that in the index template
    return render(request, "index.html", {
        "rooms": rooms,
    })